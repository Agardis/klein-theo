#include "voyageur4.h"
#include <string>
#include <iostream>

using namespace std;

//=============déclaration des fonctions de classe==================

Voyageur::Voyageur(string name, int age)											//Constructeur à arguments
{
	setName(name);																	//Permet d'initialiser directement les variables de l'objet
	setAge(age);
}

Voyageur::Voyageur()																//Constructeur sans arguments
{}																					//Utilisé pour laisser l'utilisateur initialiser les variables de l'objet

void Voyageur::setCategory(int age)
{																					//Permet d'initialiser la categorie de Voyageur
	if (age < 1)
		this->category = Nourrisson;
	else if (age < 18)
		this->category = Enfant;
	else if (age < 61)
		this->category = Adulte;
	else
		this->category = Senior;
}

int Voyageur::setAge(int age)
{
	if (age >= 0)																	//vérifie si l'age est correct, sinon renvoie false
	{
		this->age = age;
		setCategory(age);
		return 1;
	}
	return 0;
}

int Voyageur::setName(string name)
{
	if (name.length() >= 2)															//vérifie si le nom est correct, sinon renvoie false
	{
		this->name = name;
		return 1;
	}
	return 0;
}

int Voyageur::getAge()
{
	return this->age;
}

string Voyageur::getName()
{
	return this->name;
}

Voyageur::categories Voyageur::getCategory()
{
	return this->category;
}

void Voyageur::toString()															//méthode toString
{
	cout << "_________________________________" << endl;							//Permet d'afficher les informations contenues dans les variables de l'objet
	cout << " Voyageur " << getName(); << ", " << getAge(); << " ans" << endl;
	cout << " Catégorie: " << categoryToString(getCategory()) << endl;
}

string Voyageur::categoryToString(categories category)
{
	switch (category)
	{
		case 0:
			return "Nourrisson";
		case 1:
			return "Enfant";
		case 2:
			return "Adulte";
		case 3:
			return "Senior";
		default:
			return "Pas de catégorie"; 
	}
}

//=========================fonction static==========================

void userInitialisation(Voyageur *v);							

//==========================fonction main===========================

int main(int argc, char const *argv[])
{
	string name;
	int age;

	Voyageur v1("Théo Klein", 21);													//Initialisation d'un objet "Voyageur" grâce au constructeur et affichage de ses infos
	v1.toString();
	Voyageur v2;																	//Creation d'un objet "Voyageur" sans initialisation
	userInitialisation(&v2);														//Appel de la focntion d'initialisation par l'utilisateur
	v2.toString();
	return 0;
}

//================déclaration des fonctions static==================

void userInitialisation(Voyageur *v)
{
	int age;
	string name;

	cout << "_________________________________" << endl;
	cout << " Votre nom: ";
	cin >> name;
	while (!v->setName(name))														//redemande le nom tant que le nom entré est incorrect
	{
		cout << " Veuillez entrer un nom valide (2 lettres ou plus)" << endl;
		cout << " Votre nom: ";
		cin >> name;
	}
	cout << " votre age: ";
	cin >> age;
	while (!v->setAge(age))															//redemande l'age tant que l'age entré est incorrect
	{
		cout << " Veuillez entrer un age valide (valeur positive)" << endl;
		cout << " Votre age: ";
		cin >> age;
	}

}