#ifndef	VOYAGEUR_H
#define VOYAGEUR_H
#include <string>

using namespace std;

class Voyageur
{
public:
	enum categories
	{
		Nourrisson, Enfant, Adulte, Senior
	};
private:
	string name;
	int age;
	categories category;
public:
	Voyageur(string name, int age);
	Voyageur();
public:
	void toString();
	string categoryToString(categories category);
	int setAge(int age);
	int setName(string name);
	int getAge();
	string getName();
	void setCategory(int age);
	Voyageur::categories getCategory();
};

#endif
