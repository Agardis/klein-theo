#ifndef	VOYAGEUR_H
#define VOYAGEUR_H
#include <string>

using namespace std;

class Voyageur
{
private:
	string name;
	int age;
public:
	Voyageur(string name, int age);
	Voyageur();
public:
	void toString();
	int setAge(int age);
	int setName(string name);
	int getAge();
	string getName();
};

#endif
