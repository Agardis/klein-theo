#include "voyageur1.h"
#include <string>
#include <iostream>

using namespace std;

Voyageur::Voyageur(string name, int age)											//Constructeur à arguments
{
	this->name = name;																//Permet d'initialiser directement les variables de l'objet
	this->age = age;
}

Voyageur::Voyageur()																//Constructeur sans arguments
{}																					//Utilisé pour laisser l'utilisateur initialiser les variables de l'objet

void Voyageur::toString()															//méthode toString
{
	cout << "_________________________________" << endl;							//Permet d'afficher les informations contenues dans les variables de l'objet
	cout << " Voyageur " << this->name << ", " << this->age << " ans" << endl;
}


//void userInitialisation(Voyageur v);								


int main(int argc, char const *argv[])
{
	Voyageur v1("Théo Klein", 21);													//Initialisation d'un objet "Voyageur" et affichage de ses infos
	v1.toString();
	Voyageur v2;																	//Creation d'un objet "Voyageur" sans initialisation
	cout << "_________________________________" << endl;
	cout << "Votre nom: ";															//Initialisation du second objet "Voyageur"^par l'utilisateur
	cin >> v2.name;
	cout << "votre age: ";
	cin >> v2.age;
	v2.toString();
	return 0;
}

/* à utiliser avec des pointeurs
void userInitialisation(Voyageur v)
{
	cout << "_________________________________" << endl;
	cout << "Votre nom: ";
	cin >> v.name;
	cout << "votre age: ";
	cin >> v.age;
}*/
