#include "voyageur3.h"
#include <string>
#include <iostream>

using namespace std;

//=============déclaration des fonctions de classe==================

Voyageur::Voyageur(string name, int age)											//Constructeur à arguments
{
	this->name = name;																//Permet d'initialiser directement les variables de l'objet
	this->age = age;
}

Voyageur::Voyageur()																//Constructeur sans arguments
{}																					//Utilisé pour laisser l'utilisateur initialiser les variables de l'objet

void Voyageur::toString()															//méthode toString
{
	cout << "_________________________________" << endl;							//Permet d'afficher les informations contenues dans les variables de l'objet
	cout << " Voyageur " << this->name << ", " << this->age << " ans" << endl;
}

int Voyageur::setAge(int age)
{
	if (age >= 0)																	//vérifie si l'age est correct, sinon renvoie false
	{
		this->age = age;
		return 1;
	}
	return 0;
}

int Voyageur::setName(string name)
{
	if (name.length() >= 2)															//vérifie si le nom est correct, sinon renvoie false
	{
		this->name = name;
		return 1;
	}
	return 0;
}

int Voyageur::getAge()
{
	return this->age;
}

string Voyageur::getName()
{
	return this->name;
}

//=========================fonction static==========================

void userInitialisation(Voyageur *v);

void category(Voyageur *v);							

//==========================fonction main===========================

int main(int argc, char const *argv[])
{
	string name;
	int age;

	Voyageur v1("Théo Klein", 21);													//Initialisation d'un objet "Voyageur" grâce au constructeur et affichage de ses infos
	v1.toString();
	category(&v1);
	Voyageur v2;																	//Creation d'un objet "Voyageur" sans initialisation
	userInitialisation(&v2);														//Appel de la focntion d'initialisation par l'utilisateur
	v2.toString();
	category(&v2);
	return 0;
}

//================déclaration des fonctions static==================

void userInitialisation(Voyageur *v)
{
	int age;
	string name;

	cout << "_________________________________" << endl;
	cout << "Votre nom: ";
	cin >> name;
	while (!v->setName(name))														//redemande le nom tant que le nom entré est incorrect
	{
		cout << "Veuillez entrer un nom valide (2 lettres ou plus)" << endl;
		cout << "Votre nom: ";
		cin >> name;
	}
	cout << "votre age: ";
	cin >> age;
	while (!v->setAge(age))															//redemande l'age tant que l'age entré est incorrect
	{
		cout << "Veuillez entrer un age valide (valeur positive)" << endl;
		cout << "Votre age: ";
		cin >> age;
	}

}

void category(Voyageur *v)
{																					//affiche la catégorie du voyageur en fonction de son age
	cout << "Catégorie: ";
	int age =  v->getAge();
	if(age < 1)
		cout << "Nourrisson";
	else if (age < 18)
		cout << "Enfant";
	else if (age < 61)
		cout << "Adulte";
	else
		cout << "Senior";
	cout << endl;
}