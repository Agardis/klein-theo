#ifndef BAGAGES_H
#define BAGAGES_H
#include <string>

using namespace std;

class Bagage
{
private:
	enum colors
	{
		Black, White, Red, Green, Blue, Yellow, Brown, Grey, Pink, Default
	};
private:
	int number;
	float weight;
	colors color;
public:
	Bagage(int number, float weight, int color);
	Bagage();
public:
	void setNumber(int number);
	void setWeight(float weight);
	void setColor(int color);
	int getNumber();
	float getWeight();
	Bagage::colors getColor();
	void toString();
	string colorToString(colors color);
};


#endif