#include "voyageur.h"
#include <string>
#include <iostream>

using namespace std;

//=============déclaration des constructeurs de Voyageur================

Voyageur::Voyageur(string name, int age)											//Constructeur à arguments
{
	Bagage def;
	setName(name);																	//Permet d'initialiser directement les variables de l'objet
	setAge(age);
	setBagage(def);
}

Voyageur::Voyageur()																//Constructeur sans arguments
{
	Bagage def;
	setBagage(def);
}																					//Utilisé pour laisser l'utilisateur initialiser les variables de l'objet

//===================Déclaration des fonctions set======================

void Voyageur::setCategory(int age)
{																					//Permet d'initialiser la categorie de Voyageur
	if (age < 1)
		this->category = Nourrisson;
	else if (age < 18)
		this->category = Enfant;
	else if (age < 61)
		this->category = Adulte;
	else
		this->category = Senior;
}

int Voyageur::setAge(int age)
{
	if (age >= 0)																	//vérifie si l'age est correct, sinon renvoie false
	{
		this->age = age;
		setCategory(age);
		return 1;
	}
	return 0;
}

int Voyageur::setName(string name)
{
	if (name.length() >= 2)															//vérifie si le nom est correct, sinon renvoie false
	{
		this->name = name;
		return 1;
	}
	return 0;
}	

void Voyageur::setAddress(AdressePostale address)
{
	this->address = address;
}

void Voyageur::setBagage(Bagage bagage)
{
	this->bagage = bagage;
}

//===================Déclaration des fonctions get======================

int Voyageur::getAge()
{
	return this->age;
}

string Voyageur::getName()
{
	return this->name;
}

Voyageur::categories Voyageur::getCategory()
{
	return this->category;
}

AdressePostale Voyageur::getAddress()
{
	return this->address;
}

Bagage Voyageur::getBagage()
{
	return this->bagage;
}

//=====================Déclaration des fonctions========================

void Voyageur::toString()															//méthode toString
{
	cout << "_________________________________" << endl;							//Permet d'afficher les informations contenues dans les variables de l'objet
	cout << " Voyageur " << getName() << ", " << getAge() << " ans" << endl;
	cout << " Catégorie: " << categoryToString(getCategory()) << endl;
	getAddress().toString();
	cout << " Bagage: ";
	if (getBagage().getNumber() != 0)
		this->bagage.toString();
	else
		cout << "Pas de bagage enregistré" << endl;
}

string Voyageur::categoryToString(categories category)								//permet de récuperer la string correspondant à la catégorie 
{
	switch (category)
	{
		case 0:
			return "Nourrisson";
		case 1:
			return "Enfant";
		case 2:
			return "Adulte";
		case 3:
			return "Senior";
		default:
			return "Pas de catégorie"; 
	}
}