#include "adressePostale.h"
#include <string>
#include <iostream>

using namespace std;


//==========déclaration des constructeurs de AdressePostale=============


AdressePostale::AdressePostale(string address, string city, string postalCode)						//constructeur de la classe
{
	setAddress(address);
	setCity(city);
	setPostalCode(postalCode);
}

AdressePostale::AdressePostale()
{}

//===================Déclaration des fonctions set======================

void AdressePostale::setAddress(string address)				
{
	this->address = address;
}

void AdressePostale::setCity(string city)
{
	this->city = city;
}

void AdressePostale::setPostalCode(string postalCode)
{
	this->postalCode = postalCode;
}

//===================Déclaration des fonctions get======================

string AdressePostale::getAddress()
{
	return this->address;
}

string AdressePostale::getCity()
{
	return this->city;
}

string AdressePostale::getPostalCode()
{
	return this->postalCode;
}

//=====================Déclaration des fonctions========================
bool AdressePostale::isAdress()
{
	if (getAddress() == "")
		return 0;
	return 1;
}


void AdressePostale::toString()																		//permet d'afficher les infos de l'adresse
{
	cout << " Adresse: ";
	if (isAdress())
		cout << getAddress() << ", " << getPostalCode() << " " << getCity() << endl;
	else
		cout << "Pas d'adresse enregistrée" << endl;
}