#ifndef	ADRESSEPOSTALE_H
#define ADRESSEPOSTALE_H
#include <string>

using namespace std;


class AdressePostale
{
private:
	string address;
	string city;
	string postalCode;
public:
	AdressePostale(string address, string city, string postalCode);
	AdressePostale();

	void toString();
	bool isAdress();
	void setAddress(string address);
	void setCity(string city);
	void setPostalCode(string postalCode);
	string getAddress();
	string getCity();
	string getPostalCode();
};

#endif