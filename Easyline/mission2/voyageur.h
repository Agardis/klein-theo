#ifndef	VOYAGEUR_H
#define VOYAGEUR_H
#include <string>
#include "adressePostale.cpp"
#include "bagages.cpp"

using namespace std;

class Voyageur
{
private:
	enum categories										//utilisation d'un "enum" plutôt qu'un tableau de string
	{
		Nourrisson, Enfant, Adulte, Senior
	};
private:
	string name;
	int age;
	categories category;
	AdressePostale address;
	Bagage bagage;
public:
	Voyageur(string name, int age);
	Voyageur();
public:
	void toString();
	string categoryToString(categories category);
	int setAge(int age);
	int setName(string name);
	void setCategory(int age);
	void setAddress(AdressePostale address);
	void setBagage(Bagage bagage);
	int getAge();
	string getName();
	Voyageur::categories getCategory();
	AdressePostale getAddress();
	Bagage getBagage();
};

#endif
