#include "bagages.h"
#include <string>
#include <iostream>

using namespace std;

//=============déclaration des constructeurs de Voyageur================

Bagage::Bagage(int number, float weight, int color)
{
	setNumber(number);
	setWeight(weight);
	setColor(color);
}

Bagage::Bagage()
{
	setNumber(0);
	setWeight(0);
	setColor(0);
}

//===================Déclaration des fonctions set======================

void Bagage::setNumber(int number)
{
	this->number = number;
}

void Bagage::setWeight(float weight)
{
	this->weight = weight;
}

void Bagage::setColor(int color)
{
	Bagage::colors trueColor;
	switch (color)
	{
		case 1:
			trueColor = Black;
			break;
		case 2:
			trueColor = White;
			break;
		case 3:
			trueColor = Red;
			break;
		case 4:
			trueColor = Green;
			break;
		case 5:
			trueColor = Blue;
			break;
		case 6:
			trueColor = Yellow;
			break;
		case 7:
			trueColor = Brown;
			break;
		case 8:
			trueColor = Grey;
			break;
		case 9:
			trueColor = Pink;
			break;
		default:
			trueColor = Default;
			break;
	}

	this->color = trueColor;
}


//===================Déclaration des fonctions get======================

int Bagage::getNumber()
{
	return this->number;
}

float Bagage::getWeight()
{
	return this->weight;
}

Bagage::colors Bagage::getColor()
{
	return this->color;
}


//=====================Déclaration des fonctions========================


void Bagage::toString()
{
	cout << "\n\tnuméro: " << getNumber() << "\n\tpoids: " << getWeight() << "kg" << "\n\tcouleur: " << colorToString(getColor()) << endl;
}

string Bagage::colorToString(colors color)
{
	switch (color)
	{
		case 0:
			return "Noir";
		case 1:
			return "Blanc";
		case 2:
			return "Rouge";
		case 3:
			return "Vert";
		case 4:
			return "Bleu";
		case 5:
			return "Jaune";
		case 6:
			return "Marron";
		case 7:
			return "Gris";
		case 8:
			return "Rose";
		default:
			return "Pas de couleur enregistrée";
	}
}