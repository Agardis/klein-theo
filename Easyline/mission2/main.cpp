#include "voyageur.cpp"
#include <string>
#include <iostream>

using namespace std;

//========================fonctions statics=========================

void userInitialisation(Voyageur *v);							
void addBagage(Voyageur *v);
void delBagage(Voyageur *v);

//==========================fonction main===========================

int main(int argc, char const *argv[])
{
	string name;
	int age;
	int choice;

	Voyageur v1("Théo Klein", 21);													//Initialisation d'un objet "Voyageur" grâce au constructeur et affichage de ses infos

	AdressePostale ad1("12 Chemin des Fortes Terres", "Pontoise", "95300");			//Initialisation d'un objet "AdressePostale" grâce au constructeur et affichage de ses infos

	v1.setAddress(ad1);

	v1.toString();

	Voyageur v2;																	//Creation d'un objet "Voyageur" sans initialisation
	userInitialisation(&v2);														//Appel de la focntion d'initialisation par l'utilisateur
	printf("\033c");
	v2.toString();


	bool quit = false;
	do
	{
		cout << " [1]Ajouter un bagage; [2]Supprimer un bagage; [3]Quitter" << endl;
		cin >> choice;
		switch (choice)
		{
			case 1:
				addBagage(&v2);
				break;

			case 2:
				delBagage(&v2);
				break;

			case 3:
				quit = true;

			default:
				break;
		}
		printf("\033c");
		v2.toString();
	} while(!quit);
	return 0;
}

//================déclaration des fonctions statics==================

void userInitialisation(Voyageur *v)
{
	int age;
	string name;
	string address;
	string city;
	string postalCode;


	cout << "_________________________________" << endl;
	cout << " Votre nom: ";
	getline(cin, name);
	while (!v->setName(name))														//redemande le nom tant que le nom entré est incorrect
	{
		cout << " Veuillez entrer un nom valide (2 lettres ou plus)" << endl;
		cout << " Votre nom: ";
		getline(cin, name);
	}
	cout << " votre age: ";
	cin >> age;
	while (!v->setAge(age))															//redemande l'age tant que l'age entré est incorrect
	{
		cout << " Veuillez entrer un age valide (valeur positive)" << endl;
		cout << " Votre age: ";
		cin >> age;
	}


	cout << " votre numéro et nom de rue: ";										//récuperation des infos de l'adresse du voyageur
	cin.ignore();
	getline(cin, address);
	cout << " votre ville: ";
	getline(cin, city);
	cout << " votre code postal: ";
	getline(cin, postalCode);

	AdressePostale ad(address, city, postalCode);									//Initialisation de l'objet "AdressePostale" avec les infos fournis par l'utilisateur
	v->setAddress(ad);																//ajout de l'objet "AdressePostale" dans l'objet "Voyageur"
}

void addBagage(Voyageur *v)
{
	if (v->getBagage().getColor() == 9)
	{
		int number;
		float weight;
		int color;

		cout << " Quel est le numéro de votre bagage ?" << endl;
		cout << " numéro: ";
		cin >> number;
		cout << " Quel est le poids de votre bagage ? (en kg)" << endl;
		cout << " poids: ";
		cin >> weight;
		cout << " Quelle est la couleur de votre bagage ?" << endl;
		cout << " [1]Noir; [2]Blanc; [3]Rouge; [4]Vert; [5]Bleu; [6]Jaune; [7]Marron; [8]Gris; [9]Rose" << endl;
		cout << " couleur: ";
		cin >> color;
		Bagage b(number, weight, color);
		v->setBagage(b);
	}
	else
	{
		cout << " Vous avez déjà un bagage enregistré" << endl;
	}
}

void delBagage(Voyageur *v)
{
	Bagage def;
	v->setBagage(def);
	cout << " Votre bagage à été supprimé" << endl;
}