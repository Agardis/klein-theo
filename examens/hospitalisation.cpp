#include <iostream>
#include <string>

using namespace std;

float secu_pourcentage = 50;																			//variable globale du pourcentage remboursé par la sécurité sociale

float mutuelle_pourcentage = 28;																		//variable globale du pourcentage remboursé par la mutuelle

float total_price = 0;

float secu_total_price = 0;

float mutuelle_total_price = 0;

float calcul_pourcentage(float hospitalisation_price, float pourcentage);								//déclaration de la fonction qui calcule les pourcentages

int UI();																								//déclaration de la fonction qui communique avec l'utilisateur

int affichage_totaux();

int main()
{
	int nbr_calculs = 8;																				//variable contenant le nombre de calcul de prise en charge à effectuer

	for (int i = 0; i < nbr_calculs; ++i)																//boucle "for" permettant de répéter le programme "nbr_calculs fois"
	{
		cout << "#" << i+1 << endl;																		//numérotation des calculs de prise en charge
		
		UI();																							//appel de la fonction UI
		
		if(i == nbr_calculs - 1)																		
		{																								//condition permettant de savoir si l'utilisateur veut procéder à des calculs supplémentaires
			affichage_totaux();

			int continuer;																	
			cout << "Voulez-vous calculer d'autres prises en charge ?" << endl;	
			cout << "0:Non 1:Oui" << endl;
			cin >> continuer;
			if (continuer)
			{
				cout << "Combien de prises en charge supplémentaires voulez-vous calculer ?" << endl;
				cin >> continuer;
			}
			nbr_calculs += continuer;
		}
	}

	return 0;
}

int affichage_totaux()
{																										//affichage des totaux pris en charge et du reste à charge du patient
	cout << "Prise en charge totale de la Sécurité Sociale: " << secu_total_price << "€" << endl;
	cout << "Prise en charge totale de la mutuelle: " << mutuelle_total_price << "€" << endl;
	cout << "Reste à charge du patient: " << total_price << "€" << "\n" << endl;
}

int UI()
{
	float hospitalisation_price;																		//déclaration des variables permettant le calcul des droits
	float secu_price;
	float mutuelle_price;
	float patient_price;
	cout << "Quel est le montant de votre hospitalisation ?" << endl;									//récupération du montant total
	cin >> hospitalisation_price;

	secu_price = calcul_pourcentage(hospitalisation_price, secu_pourcentage);							//calcul, affichage et ajout au total du montant pris en charge par la sécurité sociale
	cout << "Prise en charge de la Sécurité Sociale: " << secu_price << "€" << endl;
	secu_total_price += secu_price;

	mutuelle_price = calcul_pourcentage(hospitalisation_price, mutuelle_pourcentage);					//calcul, affichage et ajout au total du montant pris en charge par la mutuelle
	cout << "Prise en charge de la mutuelle: " << mutuelle_price << "€" << endl;
	mutuelle_total_price += mutuelle_price;

	patient_price = hospitalisation_price - (secu_price + mutuelle_price);								//calcul, affichage et ajout au total du montant à la charge du patient
	cout << "Reste à charge du patient: " << patient_price << "€" << "\n" << endl;
	total_price += patient_price;
}

float calcul_pourcentage(float hospitalisation_price, float pourcentage)								//fonction de calcul du pourcentage
{
	return hospitalisation_price * (pourcentage/100);
}