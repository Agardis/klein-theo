#include <iostream>
#include <string>

using namespace std;

string coffee[35];
float priceAndType[35][2];

void coffeeCount(int begin, int nbrCoffee);
void saveCoffeeUI(int nbr);
void saveCoffee(string coffeeName, float coffeePrice, float coffeeType, int nbr);
void displayCoffeeUI(int nbrCoffee);
void displayMin(int nbrMaxCoffee);
void countRobusta(int nbrMaxCoffee);

int main(int argc, char const *argv[])
{
	cout << "   Bonjour, bienvenue chez Xpresso :)" << endl;
	cout << "---------------------------------------" << endl;
	coffeeCount(0, 5);
	displayMin(5);
	countRobusta(5);
	int nbrCoffee = 0;
	cout << "combien de cafés voulez-vous enregistrer en plus ? (dans un maximum de 30)" << endl;
	cin >> nbrCoffee;
	coffeeCount(5, nbrCoffee);
	return 0;
}



void coffeeCount(int begin, int nbrCoffee)
{
	for (int i = 0; i < nbrCoffee; ++i)
		saveCoffeeUI(i);
}

void saveCoffeeUI(int nbr)
{
	string coffeeName;
	float coffeePrice;
	float coffeeType;
	cout << "veuillez enregistrer un café:" << endl;
	cout << "veuillez saisir le nom, le prix et\nle type de café ([1]Arabica [2]Robusta):" << endl;
	cin >> coffeeName >> coffeePrice >> coffeeType;
	saveCoffee(coffeeName, coffeePrice, coffeeType, nbr);
}

void saveCoffee(string coffeeName, float coffeePrice, float coffeeType, int nbr)
{	
	coffee[nbr] = coffeeName;
	priceAndType[nbr][0] = coffeePrice;
	priceAndType[nbr][1] = coffeeType;
	displayCoffeeUI(nbr);
}

void displayCoffeeUI(int nbrCoffee)
{
	string tmp;
	if (priceAndType[nbrCoffee][1] == 1)
		tmp = "Arabica";
	else
		tmp = "Robusta";
	cout << "_______________________________________" << endl;
	cout << "vous avez " << nbrCoffee + 1 << " cafés enregistrés" << endl;
	cout << "dernier café enregistré:" << endl;
	cout << "---------------------------------------" << endl;
	cout << coffee[nbrCoffee] << " | " << priceAndType[nbrCoffee][0] << " € | " << tmp << endl;
	cout << "---------------------------------------" << endl;
}

void displayMin(int nbrMaxCoffee)
{
	int result = 0;

	float min = priceAndType[0][0];
	int i = 0;
	do{
		if (priceAndType[i][0] < min)
		{
			result = i;
			min = priceAndType[i][0];
		}
		i += 1;	
	}
	while(i < nbrMaxCoffee);
	cout << "le café le moins cher est " << coffee[result] << endl;
}

void countRobusta(int nbrMaxCoffee)
{
	float result = 0;
	int i = 0;
	do{
		if (priceAndType[i][1] == 2)
			result += 1;
		i += 1;
	}while(i < nbrMaxCoffee);
	cout << "il y a " << result << " Robusta" << endl;
}



