#include <iostream>
#include <string>

using namespace std;


int display(string name[], float price[], int nbrItems);

int fillCart(string name[], float price[], int nbrItems);

float calculateTotalPrice(float price[], int nbrItems);

int main(int argc, char const *argv[])
{
	int nbrItems;

	cout << "Indiquez la taille de votre panier:" << endl;
	cin >> nbrItems;

	string name[nbrItems];
	float price[nbrItems];

	fillCart(name, price, nbrItems); 
	
	display(name, price, nbrItems);

	float totalPrice = calculateTotalPrice(price, nbrItems);
	cout << "Total = " << totalPrice << "€" << endl;
	
	return 0;
}

int fillCart(string name[], float price[], int nbrItems)
{
	for (int i = 0; i < nbrItems; ++i)
	{
		cout << "Nom du produit n°" << i+1 << ": ";
		cin >> name[i];
		cout << "Prix du produit n°" << i+1 << ": ";
		cin >> price[i];
		cout << endl;
	}
}



int display(string name[], float price[], int nbrItems)
{
	cout << "Liste du panier:" << endl;

	for (int i = 0; i < nbrItems; ++i)
		cout << name[i] << " -- " << price[i] << "€" << endl;
	return 0;
}

float calculateTotalPrice(float price[], int nbrItems)
{
	float result = 0;

	for (int i = 0; i < nbrItems; ++i)
		result += price[i];
	
	return result;
}