#include <iostream>
#include <string>

using namespace std;

int nbrItems = 5;
string name[5] = {"item1", "item2", "item3", "item4", "item5"};
float price[5] = {10, 20, 30, 40, 50};



int display(int nbrItems);


int main(int argc, char const *argv[])
{
	display(nbrItems);
	return 0;
}


int display(int nbrItems)
{
	cout << "Liste du panier:" << endl;

	for (int i = 0; i < nbrItems; ++i)
		cout << name[i] << " -- " << price[i] << "€" << endl;
	return 0;
}