#include <iostream>
#include <string>

using namespace std;


int display(string name[], float price[], float priceTTC[], int nbrItems);				//Déclaration des fonctions				

int fillCart(string name[], float price[], int nbrItems);

float calculateTotalPrice(float price[], int nbrItems);

int bubbleSort(string name[], float price[], int nbrItems);

int swap(int a, int b, float price[], string name[]);

int displayMinMaxMid(string name[], float price[], int nbrItems);

float calculateMid(float price[], int nbrItems);

int calculateTTC(float price[], float priceTTC[], int nbrItems);

int main(int argc, char const *argv[])
{
	int nbrItems;																		//Déclaration de la variable contenant le nombre de produits du panier

	cout << "Indiquez la taille de votre panier:" << endl;	
	cin >> nbrItems;																	//Récupération du nombre de produits du panier

	string name[nbrItems];																//Déclaration des tableaux contenant les noms, les prix HT et les prix TTC des produits
	float price[nbrItems];
	float priceTTC[nbrItems];

	fillCart(name, price, nbrItems); 													//Appel de la fonction de remplissage du panier
	
	bubbleSort(name, price, nbrItems);													//Appel de la fonction de tri des tableaux "price" et "name"

	calculateTTC(price, priceTTC, nbrItems);											//Appel de la fonction de calcul du prix TTC

	display(name, price, priceTTC, nbrItems);											//Appel de la fonction d'affichage du panier

	float totalPrice = calculateTotalPrice(price, nbrItems);							//Récupération du prix total par appel de la fonction de calcul
	cout << "Total = " << totalPrice << "€" << endl;									//affichage du prix total
	
	displayMinMaxMid(name, price, nbrItems);											//affichage du prix minimum, maximum et moyen du panier

	return 0;
}

int fillCart(string name[], float price[], int nbrItems)								//fonction de remplissage du panier
{
	for (int i = 0; i < nbrItems; ++i)													//Remplissage des tableaux "price" et "name" en les parcourant
	{
		cout << "Nom du produit n°" << i+1 << ": ";
		cin >> name[i];
		cout << "Prix du produit n°" << i+1 << ": ";
		cin >> price[i];
		cout << endl;
	}

	return 0;
}



int display(string name[], float price[], float priceTTC[], int nbrItems)				//fonction d'affichage du panier
{
	cout << "Liste du panier:" << endl;

	for (int i = 0; i < nbrItems; ++i)
	{																					//affichage des différents éléments du panier et de leur prix 
		cout << name[i] << " est au prix hors taxes de " << price[i] << "€";			//par parcours des trois tableaux
		cout << " et au prix TTC de " << priceTTC[i] << "€" << endl;
	}
	return 0;
}

float calculateTotalPrice(float price[], int nbrItems)									//fonction de calcul du prix total
{
	float result = 0;

	for (int i = 0; i < nbrItems; ++i)													//calcul du prix total en parcourant le tableau "price"
		result += price[i];
	
	return result;																	
}

int bubbleSort(string name[], float price[], int nbrItems)								//fonction de tri de type bubble sort
{
	bool swapped;
	do
	{																					//trie chaque élément en le comparant aux autres éléments non triés
		swapped = false;																//en commencant par le maximum
		for (int i = 1; i < nbrItems; ++i)
		{
			if (price[i - 1] > price[i])
			{
				swap(i - 1, i, price, name);
				swapped = true;
			}
		}

		nbrItems -= 1;
	}while(swapped);

	return 0;
}

int swap(int a, int b, float price[], string name[])									//fonction d'échange de deux variables dans les deux tableaux
{																						//utilisée dans le bubble sort pour déplacer la valeur jusqu'à sa bonne position
	float tmpFlt = price[a];
	price[a] = price[b];
	price[b] = tmpFlt;

	string tmpStr = name[a];															//échange aussi les valeurs du tableaux "name" pour garder la correspondance entre les tableaux
	name[a] = name[b];
	name[b] = tmpStr;

	return 0;
}

int displayMinMaxMid(string name[], float price[], int nbrItems)						//fonction d'affichage du prix minimum, maximum, et moyen du panier
{
	cout << "le produit le moins cher est: " << endl;									
	cout << name[0] << " -- " << price[0] << "€" << endl;
	cout << "le produit le plus cher est: " << endl;
	cout << name[nbrItems - 1] << " -- " << price[nbrItems - 1] << "€" << endl;
	cout << "le prix moyen est: " << calculateMid(price, nbrItems) << "€" << endl;		//appel de la fonction de calcul du prix moyen
}

float calculateMid(float price[], int nbrItems)											//fonction de calcul du prix moyen par parcours du tableaux "price"
{
	float result = 0;

	for (int i = 0; i < nbrItems; ++i)
		result += price[i];

	return result / nbrItems;
}

int calculateTTC(float price[], float priceTTC[], int nbrItems)
{
	for (int i = 0; i < nbrItems; ++i)
		priceTTC[i] = price[i] + price[i] * 0.2;

	return 0;
}