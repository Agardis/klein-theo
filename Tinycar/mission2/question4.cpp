#include <iostream>
#include <string>
#include <string.h>

using namespace std;


string voiture[3][3] = {{"peugeot", "206", "208"}, {"renault", "megane", "twingo"}, {"ferrari", "enzo", "california"}}; //tableau de string pour récupérer le nom de la voiture
int calcul[3][3] = {{0, 1, 2}, {0, 3, 4}, {0, 5, 6}};																	//tableau permettant de récupérer le prix en fonction de ce que rentre l'utilisateur

float prix(int marque, int modele);																						//declaration de la fonction qui récupère le prix HT

float calcul_prix_TTC(float prix_HT, float tva);																		//déclaration de la fonction du calcul du prix TTC

float calcul_remise(float prix_TTC, float remise);																		//déclaration de la fonction du calcul de remise

int identification();

int main(int argc, char const *argv[])
{
	if(identification())																								//vérification de l'identification déclanchant ou non le reste du programme
	{
		float remise = 0;																								//déclaration des variables
		float tva = 20;
		int electrique = 0;
		int marque = 0;
		int modele = 0;
		int carte = 0;

		cout << "choix de la marque: " << endl;
		cout << "1:" << voiture[0][0] << " 2:"	<< voiture[1][0] << " 3:" << voiture[2][0] << endl;						//récuperation de la marque de la voiture
		cin >> marque;
		marque -= 1;
		cout << "choix du modèle: " << endl;
		cout << "1:" << voiture[marque][1] << " 2:" << voiture[marque][2] << endl; 										//recupération du modèle
		cin >> modele;

		cout << "Avez vous une carte de fidélité ?\n1:non 2:Carte Platinum 3:Carte Gold" << endl;						//récupération des infos de la carte et ajout de la remise à la variable 'remise'
		cin >> carte;
		if(carte == 2)
			remise += 15;
		else if(carte == 3)
			remise += 20;

		cout << "Souhaitez-vous la version électrique pour une TVA à seulement 5% ?\n1:oui 2:non" << endl;				//récupération du type de vehicule, ajustement de la TVA
		cin >> electrique;																								//et ajout de la remise à la variable 'remise'
		if(electrique == 1)
		{
			tva = 5;
			if(carte == 3)
				remise += 10;
		}

		float prix_HT = prix(marque, modele); 																			//déclaration et initialisation de la variable prix HT grâce à la fonction 'prix'

		float prix_TTC = calcul_prix_TTC(prix_HT, tva); 																//déclaration et initialisation de la variable résultat 
																														//en appelant la fonction de calcul

		if (prix_TTC >= 20000)						
		{																												//ajout de la remise pour un vehicule de plus de 20000€ à la variable 'remise'
			remise += 10;
			cout << "Pour un achat de plus de 20000€ TTC vous beneficez d'une remise de 10%" << endl;
		}

		cout << "prix HT: " << prix_HT << " €" << endl;																	//affichage du prix HT et de la TVA appliquée
		cout << "TVA: " << tva << "%" << endl;
		
		if(remise != 0)																									//affichage du pourcentage de remise et calcul de la remise
		{																												//avec la fonction 'calcul_remise'
			cout << "prix TTC avant remise: " << prix_TTC << " €" << endl;
			cout << "remise: " << remise << "%" <<endl;
			prix_TTC = calcul_remise(prix_TTC, remise);
		}
		cout << "prix TTC final: " << prix_TTC << " €" << endl;															//affichage du resultat
	}
	return 0;
}

int identification()																									//fonction d'identification
{
	char real_psswrd[] = "Dark Vador";																					//déclaration et initialisation du vrai mot de passe pour comparaison
	char usr_psswrd[50];																								//déclaration d'un buffer pour récupérer le mdp inscrit par l'utilisateur
	cout << "Mot de passe ? (indice: \"Je suis ton père !\")" << endl;
	cin.getline(usr_psswrd, 50);																						
	if(strcmp(usr_psswrd, real_psswrd))																					//comparaison du mdp utilisateur et du vrai mdp pour renvoyer un booléen
	{																													//permettant de passer à la suite de la fonction main
		cout << "Mauvais mot de passe" << endl;
		return 0;
	}
	return 1;
}

float calcul_prix_TTC(float prix_HT, float tva)																			//fonction du calcul du prix TTC
{
	return prix_HT + prix_HT * (tva/100); 																				//la tva étant un pourcantage on la divise par 100 
																														//afin de la multiplier au prix HT pour avoir la valeur à rajouter au prix HT
}

float calcul_remise(float prix_TTC, float remise)																		//fonction du calcul de la remise
{																																
	return prix_TTC - prix_TTC * (remise/100);																			//même fonctionnement que pour al fonction précédente
}																														//sauf que l'on soustrait au lieu d'ajouter

float prix(int marque, int modele)																						//FONCTION DE RÉCUPÉRATION DU PRIX HT
{
	float result = 0;
	switch (calcul[marque][modele]) 																					//utilisation du tableau de chiffre pour récupérer le prix HT grâce à un switch
	{
		case 1:
			result = 9125;
			break;

		case 2:
			result = 16800;
			break;

		case 3:
			result = 29800;
			break;

		case 4:
			result = 12000;
			break;

		case 5:
			result = 540000;
			break;

		case 6:
			result = 185284,8;
			break;

		default:
			return 0;
	}
	return result;
}