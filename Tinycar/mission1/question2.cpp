#include <iostream>
#include <string>

using namespace std;

float calcul_prix_TTC(float prix_HT, float tva);		//déclaration de la fonction du calcul du prix TTC

int main(int argc, char const *argv[])
{
	float prix_HT;										//déclaration des variables
	float tva;
	cout << "prix hors taxes: " << endl;				//récuperation des valeurs des variables
	cin >> prix_HT;
	cout << "tva: " << endl;
	cin >> tva;
	float prix_TTC = calcul_prix_TTC(prix_HT, tva); 	//déclaration et initalisation de la variable resultat 
														//en appelant la fonction de calcul
	cout << "prix TTC: " << prix_TTC << " €" << endl;	//affichage du resultat
	return 0;
}


//fonction du calcul du prix TTC
float calcul_prix_TTC(float prix_HT, float tva)
{
	return prix_HT + prix_HT * (tva/100); 	//la tva étant un pourcantage on la divise par 100 
											//afin de la multiplier au prix HT pour avoir la valeur à rajouter au prix HT
}