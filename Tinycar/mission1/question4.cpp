#include <iostream>
#include <string>

using namespace std;


string voiture[3][3] = {{"peugeot", "206", "208"}, {"renault", "megane", "twingo"}, {"ferrari", "enzo", "california"}};
int calcul[3][3] = {{0, 1, 2}, {0, 3, 4}, {0, 5, 6}};

float prix(int marque, int modele);

float calcul_prix_TTC(float prix_HT, float tva);		//déclaration de la fonction du calcul du prix TTC

int main(int argc, char const *argv[])
{
	int marque = 0;
	int modele = 0;										//déclaration de la variable qui contient le prix HT
	cout << "choix de la marque: " << endl;
	cout << "1:" << voiture[0][0] << " 2:"	<< voiture[1][0] << " 3:" << voiture[2][0] << endl;	//récuperation de la marque de la voiture
	cin >> marque;
	marque -= 1;
	cout << "choix du modèle: " << endl;
	cout << "1:" << voiture[marque][1] << " 2:" << voiture[marque][2] << endl;
	cin >> modele;

	float prix_HT = prix(marque, modele); 				//déclaration et initialisation de la variable prix HT

	float prix_TTC = calcul_prix_TTC(prix_HT, 20); 		//déclaration et initialisation de la variable résultat 
														//en appelant la fonction de calcul
	cout << "prix HT: " << prix_HT << " €" << endl;
	cout << "prix TTC: " << prix_TTC << " €" << endl;	//affichage du resultat
	return 0;
}


//fonction du calcul du prix TTC
float calcul_prix_TTC(float prix_HT, float tva)
{
	return prix_HT + prix_HT * (tva/100); 	//la tva étant un pourcantage on la divise par 100 
											//afin de la multiplier au prix HT pour avoir la valeur à rajouter au prix HT
}

float prix(int marque, int modele)
{
	float result = 0;
	switch (calcul[marque][modele])
	{
		case 1:
			result = 9125;
			break;

		case 2:
			result = 16800;
			break;

		case 3:
			result = 29800;
			break;

		case 4:
			result = 12000;
			break;

		case 5:
			result = 540000;
			break;

		case 6:
			result = 185284,8;
			break;

		default:
			return 0;
	}
	return result;
}