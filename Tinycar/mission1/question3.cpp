#include <iostream>
#include <string>

using namespace std;

float calcul_prix_TTC(float prix_HT, float tva);		//déclaration de la fonction du calcul du prix TTC

int main(int argc, char const *argv[])
{
	float prix_HT;										//déclaration de la variable qui contient le prix HT
	cout << "prix hors taxes: " << endl;				//récuperation de la valeur de la variable
	cin >> prix_HT;

	float prix_TTC = calcul_prix_TTC(prix_HT, 20); 		//déclaration et initalisation de la variable résultat 
														//en appelant la fonction de calcul
	
	cout << "prix TTC: " << prix_TTC << " €" << endl;	//affichage du resultat
	return 0;
}


//fonction du calcul du prix TTC
float calcul_prix_TTC(float prix_HT, float tva)
{
	return prix_HT + prix_HT * (tva/100); 	//la tva étant un pourcantage on la divise par 100 
											//afin de la multiplier au prix HT pour avoir la valeur à rajouter au prix HT
}